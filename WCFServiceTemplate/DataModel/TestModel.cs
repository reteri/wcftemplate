namespace WCFServiceTemplate.DataModel
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using WCFServiceTemplate.Config;

   // [DbConfigurationType(typeof(WriteOracleToWebConfig))]
    public partial class TestModel : DbContext
    {
        public TestModel()
            : base("name=TestModel")
        {
        }

        public virtual DbSet<ELASTICPERSONALRECORD> ELASTICPERSONALRECORDs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ELASTICPERSONALRECORD>()
                .Property(e => e.ID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<ELASTICPERSONALRECORD>()
                .Property(e => e.FIRSTNAME)
                .IsUnicode(false);

            modelBuilder.Entity<ELASTICPERSONALRECORD>()
                .Property(e => e.MIDDLENAME)
                .IsUnicode(false);

            modelBuilder.Entity<ELASTICPERSONALRECORD>()
                .Property(e => e.LASTNAME)
                .IsUnicode(false);

            modelBuilder.Entity<ELASTICPERSONALRECORD>()
                .Property(e => e.DOB)
                .IsUnicode(false);

            modelBuilder.Entity<ELASTICPERSONALRECORD>()
                .Property(e => e.BIRTHPLACE)
                .IsUnicode(false);
        }
    }
}
