namespace WCFServiceTemplate.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ROLANDEDEVELOPER.ELASTICPERSONALRECORD")]
    public partial class ELASTICPERSONALRECORD
    {
        public decimal ID { get; set; }

        [StringLength(50)]
        public string FIRSTNAME { get; set; }

        [StringLength(50)]
        public string MIDDLENAME { get; set; }

        [StringLength(50)]
        public string LASTNAME { get; set; }

        [StringLength(20)]
        public string DOB { get; set; }

        [StringLength(50)]
        public string BIRTHPLACE { get; set; }
    }
}
