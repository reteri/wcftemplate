﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WCFServiceTemplate.DataModel;
using Oracle.ManagedDataAccess.EntityFramework;
using Oracle.ManagedDataAccess.Client;

namespace WCFServiceTemplate.Config
{
    public class WriteOracleToWebConfig : DbConfiguration
    {
        public WriteOracleToWebConfig()
        {
            this.SetDefaultConnectionFactory(new System.Data.Entity.Infrastructure.LocalDbConnectionFactory("v12.0","DATA SOURCE=oracle;PASSWORD=developer;USER ID=ROLANDEDEVELOPER"));
            this.SetProviderServices("Oracle.ManagedDataAccess.Client", EFOracleProviderServices.Instance);
            this.SetProviderFactory("Oracle.ManagedDataAccess.Client", OracleClientFactory.Instance);

        }
    }
}