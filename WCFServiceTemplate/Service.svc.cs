﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WCFServiceTemplate.DataModel;

namespace WCFServiceTemplate
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service : IService
    {
        public string GetData(int value)
        {

            Console.WriteLine("You entered: {0}", value);

            string fullname = null;
            using(var db = new TestModel())
            {
                var query = from u in db.ELASTICPERSONALRECORDs
                            where u.ID == value
                            select u;
                foreach(var item in query)
                {
                    fullname = item.FIRSTNAME + " " + item.LASTNAME;
                }
            }
            return (fullname);

        }

       
    }
}
